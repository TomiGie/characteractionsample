package jp.itnav.characteractionsample;

import android.annotation.TargetApi;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import java.util.ArrayList;

public class SoundActivity extends AppCompatActivity {
    private SoundPool soundPool;
    private ArrayList<Integer> soundIdList;
    private int[] soundResources = {
            R.raw.character_voice01,
            R.raw.character_voice02,
            R.raw.character_voice03,
            R.raw.character_voice04,
            R.raw.character_voice05,
            R.raw.character_voice06,
            R.raw.character_voice07,
            R.raw.character_voice08
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound);
        loadSoundMedia();
    }

    private void loadSoundMedia() {
        soundPool = buildSoundPool(10);
        soundIdList = new ArrayList<>();
        for (int soundResource : soundResources) {
            soundIdList.add(soundPool.load(this, soundResource, 1));
        }
    }

    public void playSound(View v) {
        int id = v.getId();
        int soundId = 0;
        switch (id) {
            case R.id.sound_01:
                soundId = soundIdList.get(0);
                break;
            case R.id.sound_02:
                soundId = soundIdList.get(1);
                break;
            case R.id.sound_03:
                soundId = soundIdList.get(2);
                break;
            case R.id.sound_04:
                soundId = soundIdList.get(3);
                break;
            case R.id.sound_05:
                soundId = soundIdList.get(4);
                break;
            case R.id.sound_06:
                soundId = soundIdList.get(5);
                break;
            case R.id.sound_07:
                soundId = soundIdList.get(6);
                break;
            case R.id.sound_08:
                soundId = soundIdList.get(7);
                break;
        }
        soundPool.play(soundId, 1, 1, 0, 0, 1);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private SoundPool buildSoundPool(int poolMax) {
        SoundPool pool;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            pool = new SoundPool(poolMax, AudioManager.STREAM_MUSIC, 0);
        }
        else {
            AudioAttributes attr = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();

            pool = new SoundPool.Builder()
                    .setAudioAttributes(attr)
                    .setMaxStreams(poolMax)
                    .build();
        }
        return pool;
    }
}
