package jp.itnav.characteractionsample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class AnimationActivity extends AppCompatActivity {

    ImageView imageView;
    Animation animation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animation);
        imageView = (ImageView)findViewById(R.id.image);
    }

    public void selectAnim(View view) {
        switch (view.getId()){
            case R.id.button1:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_01);
                imageView.startAnimation(animation);
                break;
            case R.id.button2:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_02);
                imageView.startAnimation(animation);
                break;
            case R.id.button3:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_03);
                imageView.startAnimation(animation);
                break;
            case R.id.button4:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_04);
                imageView.startAnimation(animation);
                break;
            case R.id.button5:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_05);
                imageView.startAnimation(animation);
                break;
            case R.id.button6:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_06);
                imageView.startAnimation(animation);
                break;
            case R.id.button7:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_07);
                imageView.startAnimation(animation);
                break;
            case R.id.button8:
                animation = AnimationUtils.loadAnimation(this, R.anim.anim_monster_08);
                imageView.startAnimation(animation);
                break;
        }

    }
}
